-- KSENROLL-14374

-- configure parameter and parameter group join table
INSERT INTO KSEN_GES_PARM_GRP_JN_GES_PARM (GES_PARM_GRP_ID, GES_PARM_ID)
  VALUES ('kuali.ges.parametergroup.key.rollover', 'kuali.ges.parameter.key.rollover.roomassignment.include')
/
INSERT INTO KSEN_GES_PARM_GRP_JN_GES_PARM (GES_PARM_GRP_ID, GES_PARM_ID)
  VALUES ('kuali.ges.parametergroup.key.rollover', 'kuali.ges.parameter.key.rollover.allschedulinginformation.include')
/
INSERT INTO KSEN_GES_PARM_GRP_JN_GES_PARM (GES_PARM_GRP_ID, GES_PARM_ID)
  VALUES ('kuali.ges.parametergroup.key.rollover', 'kuali.ges.parameter.key.rollover.instructorinformation.include')
/
INSERT INTO KSEN_GES_PARM_GRP_JN_GES_PARM (GES_PARM_GRP_ID, GES_PARM_ID)
  VALUES ('kuali.ges.parametergroup.key.rollover', 'kuali.ges.parameter.key.rollover.activityofferingsstateofcancelled.include')
/
INSERT INTO KSEN_GES_PARM_GRP_JN_GES_PARM (GES_PARM_GRP_ID, GES_PARM_ID)
  VALUES ('kuali.ges.parametergroup.key.rollover', 'kuali.ges.parameter.key.rollover.activityofferingscolocation.include')
/
INSERT INTO KSEN_GES_PARM_GRP_JN_GES_PARM (GES_PARM_GRP_ID, GES_PARM_ID)
  VALUES ('kuali.ges.parametergroup.key.rollover', 'kuali.ges.parameter.key.rollover.cluversions.include')
/
INSERT INTO KSEN_GES_PARM_GRP_JN_GES_PARM (GES_PARM_GRP_ID, GES_PARM_ID)
  VALUES ('kuali.ges.parametergroup.key.rollover', 'kuali.ges.parameter.key.rollover.requisitesaddedincourseoffering.include')
/