List of third-party dependencies grouped by their license type.

Apache License 2.0 : 
  * Metrics Core (io.dropwizard.metrics:metrics-core:3.2.1 - http://metrics.codahale.com/metrics-core/)
  * Metrics Health Checks (io.dropwizard.metrics:metrics-healthchecks:3.2.1 - http://metrics.codahale.com/metrics-healthchecks/)
  * JVM Integration for Metrics (io.dropwizard.metrics:metrics-jvm:3.2.1 - http://metrics.codahale.com/metrics-jvm/)
  * Javassist (org.javassist:javassist:3.21.0-GA - http://www.javassist.org/)

Apache Software License - Version 2.0 : 
  * avalon-framework (avalon-framework:avalon-framework:4.1.5 - no url defined)
  * cglib-nodep (cglib:cglib-nodep:3.2.5 - https://github.com/cglib/cglib/cglib-nodep)
  * AWS Java SDK for AWS CloudFormation (com.amazonaws:aws-java-sdk-cloudformation:1.11.18 - https://aws.amazon.com/sdkforjava)
  * AWS SDK for Java - Core (com.amazonaws:aws-java-sdk-core:1.11.18 - https://aws.amazon.com/sdkforjava)
  * AWS Java SDK for Amazon EC2 (com.amazonaws:aws-java-sdk-ec2:1.11.18 - https://aws.amazon.com/sdkforjava)
  * AWS Java SDK for AWS KMS (com.amazonaws:aws-java-sdk-kms:1.11.18 - https://aws.amazon.com/sdkforjava)
  * AWS Java SDK for Amazon S3 (com.amazonaws:aws-java-sdk-s3:1.11.18 - https://aws.amazon.com/sdkforjava)
  * Jackson-annotations (com.fasterxml.jackson.core:jackson-annotations:2.8.11 - http://github.com/FasterXML/jackson)
  * Jackson-core (com.fasterxml.jackson.core:jackson-core:2.8.11 - https://github.com/FasterXML/jackson-core)
  * jackson-databind (com.fasterxml.jackson.core:jackson-databind:2.8.11 - http://github.com/FasterXML/jackson)
  * Jackson dataformat: CBOR (com.fasterxml.jackson.dataformat:jackson-dataformat-cbor:2.8.11 - http://github.com/FasterXML/jackson-dataformats-binary)
  * Jackson-datatype-Joda (com.fasterxml.jackson.datatype:jackson-datatype-joda:2.8.11 - http://wiki.fasterxml.com/JacksonModuleJoda)
  * Jackson-JAXRS-base (com.fasterxml.jackson.jaxrs:jackson-jaxrs-base:2.8.11 - http://github.com/FasterXML/jackson-jaxrs-providers/jackson-jaxrs-base)
  * Jackson-JAXRS-JSON (com.fasterxml.jackson.jaxrs:jackson-jaxrs-json-provider:2.8.11 - http://github.com/FasterXML/jackson-jaxrs-providers/jackson-jaxrs-json-provider)
  * Jackson module: JAXB-annotations (com.fasterxml.jackson.module:jackson-module-jaxb-annotations:2.8.11 - http://github.com/FasterXML/jackson-module-jaxb-annotations)
  * Guava: Google Core Libraries for Java (com.google.guava:guava:21.0 - https://github.com/google/guava/guava)
  * Apache Commons BeanUtils (commons-beanutils:commons-beanutils:1.9.3 - https://commons.apache.org/proper/commons-beanutils/)
  * Commons Chain (commons-chain:commons-chain:1.2 - http://commons.apache.org/chain/)
  * Apache Commons Codec (commons-codec:commons-codec:1.10 - http://commons.apache.org/proper/commons-codec/)
  * Apache Commons Collections (commons-collections:commons-collections:3.2.2 - http://commons.apache.org/collections/)
  * Apache Commons Configuration (commons-configuration:commons-configuration:1.10 - http://commons.apache.org/configuration/)
  * Commons DBCP (commons-dbcp:commons-dbcp:1.4 - http://commons.apache.org/dbcp/)
  * Commons Digester (commons-digester:commons-digester:2.1 - http://commons.apache.org/digester/)
  * Apache Commons FileUpload (commons-fileupload:commons-fileupload:1.3.2 - http://commons.apache.org/proper/commons-fileupload/)
  * Apache Commons IO (commons-io:commons-io:2.5 - http://commons.apache.org/proper/commons-io/)
  * Commons Lang (commons-lang:commons-lang:2.6 - http://commons.apache.org/lang/)
  * Commons Pool (commons-pool:commons-pool:1.6 - http://commons.apache.org/pool/)
  * Apache Commons Validator (commons-validator:commons-validator:1.6 - http://commons.apache.org/proper/commons-validator/)
  * Bean Validation API (javax.validation:validation-api:1.1.0.Final - http://beanvalidation.org)
  * Joda-Time (joda-time:joda-time:2.9.7 - http://www.joda.org/joda-time/)
  * Joda-Time JSP tags support (joda-time:joda-time-jsptags:1.1.1 - http://joda-time.sourceforge.net/contrib/jsptags)
  * Apache Log4j (log4j:log4j:1.2.17 - http://logging.apache.org/log4j/1.2/)
  * logkit (logkit:logkit:1.0.1 - no url defined)
  * ehcache (net.sf.ehcache:ehcache:2.10.4 - http://ehcache.org)
  * java-support (net.shibboleth.utilities:java-support:7.1.1 - http://shibboleth.net/java-support/)
  * Apache Commons Lang (org.apache.commons:commons-lang3:3.5 - http://commons.apache.org/proper/commons-lang/)
  * Transaction (org.apache.commons.transaction:commons-transaction:1.1 - http://jakarta.apache.org/commons/${pom.artifactId.substring(8)}/)
  * Apache CXF Core (org.apache.cxf:cxf-core:3.1.10 - http://cxf.apache.org)
  * Apache CXF Runtime SOAP Binding (org.apache.cxf:cxf-rt-bindings-soap:3.1.10 - http://cxf.apache.org)
  * Apache CXF Runtime XML Binding (org.apache.cxf:cxf-rt-bindings-xml:3.1.10 - http://cxf.apache.org)
  * Apache CXF Runtime Aegis Databinding (org.apache.cxf:cxf-rt-databinding-aegis:3.1.10 - http://cxf.apache.org/)
  * Apache CXF Runtime JAXB DataBinding (org.apache.cxf:cxf-rt-databinding-jaxb:3.1.10 - http://cxf.apache.org)
  * Apache CXF Runtime JAX-RS Frontend (org.apache.cxf:cxf-rt-frontend-jaxrs:3.1.10 - http://cxf.apache.org)
  * Apache CXF Runtime JAX-WS Frontend (org.apache.cxf:cxf-rt-frontend-jaxws:3.1.10 - http://cxf.apache.org)
  * Apache CXF Runtime Simple Frontend (org.apache.cxf:cxf-rt-frontend-simple:3.1.10 - http://cxf.apache.org)
  * Apache CXF JAX-RS Client (org.apache.cxf:cxf-rt-rs-client:3.1.10 - http://cxf.apache.org)
  * Apache CXF Runtime Security functionality (org.apache.cxf:cxf-rt-security:3.1.10 - http://cxf.apache.org)
  * Apache CXF Runtime SAML Security functionality (org.apache.cxf:cxf-rt-security-saml:3.1.10 - http://cxf.apache.org)
  * Apache CXF Runtime HTTP Transport (org.apache.cxf:cxf-rt-transports-http:3.1.10 - http://cxf.apache.org)
  * Apache CXF Runtime HTTP Jetty Transport (org.apache.cxf:cxf-rt-transports-http-jetty:3.1.10 - http://cxf.apache.org)
  * Apache CXF Runtime WS Addressing (org.apache.cxf:cxf-rt-ws-addr:3.1.10 - http://cxf.apache.org)
  * Apache CXF Runtime WS Policy (org.apache.cxf:cxf-rt-ws-policy:3.1.10 - http://cxf.apache.org)
  * Apache CXF Runtime WS Security (org.apache.cxf:cxf-rt-ws-security:3.1.10 - http://cxf.apache.org)
  * Apache CXF Runtime Core for WSDL (org.apache.cxf:cxf-rt-wsdl:3.1.10 - http://cxf.apache.org)
  * Apache HttpClient (org.apache.httpcomponents:httpclient:4.5.3 - http://hc.apache.org/httpcomponents-client)
  * Apache HttpCore (org.apache.httpcomponents:httpcore:4.4.5 - http://hc.apache.org/httpcomponents-core-ga)
  * Apache Neethi (org.apache.neethi:neethi:3.0.3 - http://ws.apache.org/neethi/)
  * Apache XML Security for Java (org.apache.santuario:xmlsec:2.0.8 - http://santuario.apache.org/)
  * Struts Core (org.apache.struts:struts-core:1.3.11-kuali-1 - http://struts.apache.org)
  * Struts EL (org.apache.struts:struts-el:1.3.11-kuali-1 - http://struts.apache.org)
  * Struts Extras (org.apache.struts:struts-extras:1.3.11-kuali-1 - http://struts.apache.org)
  * Struts Taglib (org.apache.struts:struts-taglib:1.3.11-kuali-1 - http://struts.apache.org)
  * Apache Velocity (org.apache.velocity:velocity:1.7 - http://velocity.apache.org/engine/devel/)
  * VelocityTools (org.apache.velocity:velocity-tools:2.0 - http://velocity.apache.org/tools/devel/)
  * XmlSchema Core (org.apache.ws.xmlschema:xmlschema-core:2.2.1 - http://ws.apache.org/commons/xmlschema20/xmlschema-core/)
  * Apache WSS4J WS-Security Bindings (org.apache.wss4j:wss4j-bindings:2.1.8 - http://ws.apache.org/wss4j/wss4j-bindings/)
  * Apache WSS4J WS-SecurityPolicy model (org.apache.wss4j:wss4j-policy:2.1.8 - http://ws.apache.org/wss4j/wss4j-policy/)
  * Apache WSS4J WS-Security Common (org.apache.wss4j:wss4j-ws-security-common:2.1.8 - http://ws.apache.org/wss4j/wss4j-ws-security-common/)
  * Apache WSS4J DOM WS-Security (org.apache.wss4j:wss4j-ws-security-dom:2.1.8 - http://ws.apache.org/wss4j/wss4j-ws-security-dom/)
  * Apache WSS4J Streaming WS-SecurityPolicy (org.apache.wss4j:wss4j-ws-security-policy-stax:2.1.8 - http://ws.apache.org/wss4j/wss4j-ws-security-policy-stax/)
  * Apache WSS4J Streaming WS-Security (org.apache.wss4j:wss4j-ws-security-stax:2.1.8 - http://ws.apache.org/wss4j/wss4j-ws-security-stax/)
  * Woodstox (org.codehaus.woodstox:woodstox-core-asl:4.4.1 - http://woodstox.codehaus.org)
  * Cryptacular Library (org.cryptacular:cryptacular:1.0 - http://www.cryptacular.org)
  * dwr (org.directwebremoting:dwr:3.0.RC2 - no url defined)
  * Jetty :: Continuation (org.eclipse.jetty:jetty-continuation:9.2.18.v20160721 - http://www.eclipse.org/jetty)
  * Jetty :: Http Utility (org.eclipse.jetty:jetty-http:9.4.2.v20170220 - http://www.eclipse.org/jetty)
  * Jetty :: IO Utility (org.eclipse.jetty:jetty-io:9.4.2.v20170220 - http://www.eclipse.org/jetty)
  * Jetty :: Server Core (org.eclipse.jetty:jetty-server:9.4.2.v20170220 - http://www.eclipse.org/jetty)
  * Jetty :: Utilities (org.eclipse.jetty:jetty-util:9.4.2.v20170220 - http://www.eclipse.org/jetty)
  * Apache FreeMarker (org.freemarker:freemarker:2.3.25-incubating - http://freemarker.org/)
  * JASYPT: Java Simplified Encryption (org.jasypt:jasypt:1.9.2 - http://www.jasypt.org)
  * Objenesis (org.objenesis:objenesis:2.1 - http://objenesis.org)
  * OpenSAML :: Core (org.opensaml:opensaml-core:3.1.1 - http://shibboleth.net/opensaml-core/)
  * OpenSAML :: Profile API (org.opensaml:opensaml-profile-api:3.1.1 - http://shibboleth.net/opensaml-profile-api/)
  * OpenSAML :: SAML Provider API (org.opensaml:opensaml-saml-api:3.1.1 - http://shibboleth.net/opensaml-saml-api/)
  * OpenSAML :: SAML Provider Implementations (org.opensaml:opensaml-saml-impl:3.1.1 - http://shibboleth.net/opensaml-saml-impl/)
  * OpenSAML :: Security API (org.opensaml:opensaml-security-api:3.1.1 - http://shibboleth.net/opensaml-security-api/)
  * OpenSAML :: Security Implementation (org.opensaml:opensaml-security-impl:3.1.1 - http://shibboleth.net/opensaml-security-impl/)
  * OpenSAML :: SOAP Provider API (org.opensaml:opensaml-soap-api:3.1.1 - http://shibboleth.net/opensaml-soap-api/)
  * OpenSAML :: XACML Provider API (org.opensaml:opensaml-xacml-api:3.1.1 - http://shibboleth.net/opensaml-xacml-api/)
  * OpenSAML :: XACML Provider Implementations (org.opensaml:opensaml-xacml-impl:3.1.1 - http://shibboleth.net/opensaml-xacml-impl/)
  * OpenSAML :: SAML XACML Profile API (org.opensaml:opensaml-xacml-saml-api:3.1.1 - http://shibboleth.net/opensaml-xacml-saml-api/)
  * OpenSAML :: SAML XACML Profile Implementation (org.opensaml:opensaml-xacml-saml-impl:3.1.1 - http://shibboleth.net/opensaml-xacml-saml-impl/)
  * OpenSAML :: XML Security API (org.opensaml:opensaml-xmlsec-api:3.1.1 - http://shibboleth.net/opensaml-xmlsec-api/)
  * OpenSAML :: XML Security Implementation (org.opensaml:opensaml-xmlsec-impl:3.1.1 - http://shibboleth.net/opensaml-xmlsec-impl/)
  * quartz (org.quartz-scheduler:quartz:2.2.3 - http://www.quartz-scheduler.org/quartz)
  * Spring AOP (org.springframework:spring-aop:4.3.7.RELEASE - https://github.com/spring-projects/spring-framework)
  * Spring Beans (org.springframework:spring-beans:4.3.7.RELEASE - https://github.com/spring-projects/spring-framework)
  * Spring Context (org.springframework:spring-context:4.3.7.RELEASE - https://github.com/spring-projects/spring-framework)
  * Spring Context Support (org.springframework:spring-context-support:4.3.7.RELEASE - https://github.com/spring-projects/spring-framework)
  * Spring Core (org.springframework:spring-core:4.3.7.RELEASE - https://github.com/spring-projects/spring-framework)
  * Spring Expression Language (SpEL) (org.springframework:spring-expression:4.3.7.RELEASE - https://github.com/spring-projects/spring-framework)
  * Spring JDBC (org.springframework:spring-jdbc:4.3.7.RELEASE - https://github.com/spring-projects/spring-framework)
  * Spring Object/Relational Mapping (org.springframework:spring-orm:4.3.7.RELEASE - https://github.com/spring-projects/spring-framework)
  * Spring TestContext Framework (org.springframework:spring-test:4.3.7.RELEASE - https://github.com/spring-projects/spring-framework)
  * Spring Transaction (org.springframework:spring-tx:4.3.7.RELEASE - https://github.com/spring-projects/spring-framework)
  * Spring Web (org.springframework:spring-web:4.3.7.RELEASE - https://github.com/spring-projects/spring-framework)
  * Spring Web MVC (org.springframework:spring-webmvc:4.3.7.RELEASE - https://github.com/spring-projects/spring-framework)
  * Spring Cloud AWS Context Module (org.springframework.cloud:spring-cloud-aws-context:1.1.3.RELEASE - http://spring.io/spring-cloud/spring-cloud-aws/spring-cloud-aws-context)
  * Spring Cloud AWS Core Module (org.springframework.cloud:spring-cloud-aws-core:1.1.3.RELEASE - http://spring.io/spring-cloud/spring-cloud-aws/spring-cloud-aws-core)
  * spring-ldap-core (org.springframework.ldap:spring-ldap-core:2.3.1.RELEASE - http://www.springframework.org/ldap)
  * spring-security-core (org.springframework.security:spring-security-core:4.2.2.RELEASE - http://spring.io/spring-security)
  * spring-security-ldap (org.springframework.security:spring-security-ldap:4.0.4.RELEASE - http://spring.io/spring-security)
  * spring-modules-ojb (org.springmodules:spring-modules-ojb:0.8a - https://springmodules.dev.java.net/)
  * oro (oro:oro:2.0.8 - no url defined)
  * XML Commons Resolver Component (xml-resolver:xml-resolver:1.2 - http://xml.apache.org/commons/components/resolver/)

Apache Software License, Version 1.1 : 
  * sslext (sslext:sslext:1.2-0 - no url defined)

Artistic License : 
  * Display tag library (displaytag:displaytag:1.2 - http://displaytag.sourceforge.net/displaytag)

BSD License : 
  * XStream Core (com.thoughtworks.xstream:xstream:1.4.9 - http://x-stream.github.io/xstream)
  * dom4j (dom4j:dom4j:1.6.1 - http://dom4j.org)
  * Stax2 API (org.codehaus.woodstox:stax2-api:3.1.4 - http://wiki.fasterxml.com/WoodstoxStax2)
  * HOWL logger (org.objectweb.howl:howl:1.0.1-1 - http://forge.objectweb.org/projects/howl/)
  * ASM Core (org.ow2.asm:asm:5.2 - http://asm.objectweb.org/asm/)
  * ESAPI (org.owasp.esapi:esapi:2.1.0.1 - https://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API)

Bouncy Castle Licence : 
  * Bouncy Castle Provider (org.bouncycastle:bcprov-jdk15on:1.56 - http://www.bouncycastle.org/java.html)

CDDL - Version 1.0 : 
  * jsr311-api (javax.ws.rs:jsr311-api:1.1.1 - https://jsr311.dev.java.net)

CDDL - Version 1.1 : 
  * javax.ws.rs-api (javax.ws.rs:javax.ws.rs-api:2.0.1 - http://jax-rs-spec.java.net)

CDDL - Version 1.1 / GNU General Public License - Version 2 : 
  * javax.annotation API (javax.annotation:javax.annotation-api:1.2 - http://jcp.org/en/jsr/detail?id=250)
  * Java Servlet API (javax.servlet:javax.servlet-api:3.0.1 - http://servlet-spec.java.net)
  * JavaServer Pages(TM) Standard Tag Library (javax.servlet.jsp.jstl:jstl-api:1.2 - http://jcp.org/en/jsr/detail?id=52)
  * HK2 API module (org.glassfish.hk2:hk2-api:2.5.0-b32 - https://hk2.java.net/hk2-api)
  * ServiceLocator Default Implementation (org.glassfish.hk2:hk2-locator:2.5.0-b32 - https://hk2.java.net/hk2-locator)
  * HK2 Implementation Utilities (org.glassfish.hk2:hk2-utils:2.5.0-b32 - https://hk2.java.net/hk2-utils)
  * OSGi resource locator bundle - used by various API providers that rely on META-INF/services mechanism to locate providers. (org.glassfish.hk2:osgi-resource-locator:1.0.1 - http://glassfish.org/osgi-resource-locator/)
  * aopalliance version 1.0 repackaged as a module (org.glassfish.hk2.external:aopalliance-repackaged:2.5.0-b32 - https://hk2.java.net/external/aopalliance-repackaged)
  * javax.inject:1 as OSGi bundle (org.glassfish.hk2.external:javax.inject:2.5.0-b32 - https://hk2.java.net/external/javax.inject)
  * JavaServer Pages (TM) TagLib Implementation (org.glassfish.web:jstl-impl:1.2 - http://jstl.java.net/jstl-impl)

CDDL+GPL License : 
  * Old JAXB Core (com.sun.xml.bind:jaxb-core:2.2.11 - http://jaxb.java.net/jaxb-bundles/jaxb-core)
  * Old JAXB Runtime (com.sun.xml.bind:jaxb-impl:2.2.11 - http://jaxb.java.net/jaxb-bundles/jaxb-impl)
  * jersey-repackaged-guava (org.glassfish.jersey.bundles.repackaged:jersey-guava:2.25.1 - https://jersey.java.net/project/project/jersey-guava/)
  * jersey-core-client (org.glassfish.jersey.core:jersey-client:2.25.1 - https://jersey.java.net/jersey-client/)
  * jersey-core-common (org.glassfish.jersey.core:jersey-common:2.25.1 - https://jersey.java.net/jersey-common/)
  * jersey-ext-entity-filtering (org.glassfish.jersey.ext:jersey-entity-filtering:2.22.2 - https://jersey.java.net/project/jersey-entity-filtering/)
  * jersey-media-json-jackson (org.glassfish.jersey.media:jersey-media-json-jackson:2.22.2 - https://jersey.java.net/project/jersey-media-json-jackson/)

CDDL/GPLv2+CE : 
  * JavaMail API (com.sun.mail:javax.mail:1.5.6 - http://javamail.java.net/javax.mail)

Common Public License - Version 1.0 : 
  * WSDL4J (wsdl4j:wsdl4j:1.6.3 - http://sf.net/projects/wsdl4j)

Creative Commons 3.0 BY-SA : 
  * ESAPI (org.owasp.esapi:esapi:2.1.0.1 - https://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API)

Dual license consisting of the CDDL v1.1 and GPL v2 : 
  * JSR 353 (JSON Processing) API (javax.json:javax.json-api:1.0 - http://json-processing-spec.java.net)
  * JSR 353 (JSON Processing) Default Provider (org.glassfish:javax.json:1.0.4 - http://jsonp.java.net)

Eclipse Distribution License v. 1.0 : 
  * Javax Persistence (org.eclipse.persistence:javax.persistence:2.1.1 - http://www.eclipse.org/eclipselink)
  * EclipseLink ANTLR (org.eclipse.persistence:org.eclipse.persistence.antlr:2.6.4 - http://www.eclipse.org/eclipselink)
  * EclipseLink ASM (org.eclipse.persistence:org.eclipse.persistence.asm:2.6.4 - http://www.eclipse.org/eclipselink)
  * EclipseLink Core (org.eclipse.persistence:org.eclipse.persistence.core:2.6.4 - http://www.eclipse.org/eclipselink)
  * EclipseLink JPA (org.eclipse.persistence:org.eclipse.persistence.jpa:2.6.4 - http://www.eclipse.org/eclipselink)
  * EclipseLink Hermes JPQL Parser (org.eclipse.persistence:org.eclipse.persistence.jpa.jpql:2.6.4 - http://www.eclipse.org/eclipselink)
  * EclipseLink Oracle Extensions (org.eclipse.persistence:org.eclipse.persistence.oracle:2.6.4 - http://www.eclipse.org/eclipselink)

Eclipse Public License - Version 1.0 : 
  * AspectJ weaver (org.aspectj:aspectjweaver:1.8.10 - http://www.aspectj.org)
  * Jetty :: Continuation (org.eclipse.jetty:jetty-continuation:9.2.18.v20160721 - http://www.eclipse.org/jetty)
  * Jetty :: Http Utility (org.eclipse.jetty:jetty-http:9.4.2.v20170220 - http://www.eclipse.org/jetty)
  * Jetty :: IO Utility (org.eclipse.jetty:jetty-io:9.4.2.v20170220 - http://www.eclipse.org/jetty)
  * Jetty :: Server Core (org.eclipse.jetty:jetty-server:9.4.2.v20170220 - http://www.eclipse.org/jetty)
  * Jetty :: Utilities (org.eclipse.jetty:jetty-util:9.4.2.v20170220 - http://www.eclipse.org/jetty)

Eclipse Public License 1.0 : 
  * JUnit (junit:junit:4.12 - http://junit.org)

Eclipse Public License v1.0 : 
  * Javax Persistence (org.eclipse.persistence:javax.persistence:2.1.1 - http://www.eclipse.org/eclipselink)
  * EclipseLink ANTLR (org.eclipse.persistence:org.eclipse.persistence.antlr:2.6.4 - http://www.eclipse.org/eclipselink)
  * EclipseLink ASM (org.eclipse.persistence:org.eclipse.persistence.asm:2.6.4 - http://www.eclipse.org/eclipselink)
  * EclipseLink Core (org.eclipse.persistence:org.eclipse.persistence.core:2.6.4 - http://www.eclipse.org/eclipselink)
  * EclipseLink JPA (org.eclipse.persistence:org.eclipse.persistence.jpa:2.6.4 - http://www.eclipse.org/eclipselink)
  * EclipseLink Hermes JPQL Parser (org.eclipse.persistence:org.eclipse.persistence.jpa.jpql:2.6.4 - http://www.eclipse.org/eclipselink)
  * EclipseLink Oracle Extensions (org.eclipse.persistence:org.eclipse.persistence.oracle:2.6.4 - http://www.eclipse.org/eclipselink)

Educational Community License - Version 2.0 : 
  * ObJectRelationalBridge (org.kuali.db.ojb:db-ojb:1.0.4-patch9 - http://site.kuali.org/db/ojb/db-ojb/1.0.4-patch9)
  * Kuali JOTM (org.kuali.jotm:jotm-core:2.1.10-kuali-1 - http://site.kuali.org/jotm/jotm-core/2.1.10-kuali-1)
  * Rice Core API (org.kuali.rice:rice-core-api:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-core/rice-core-api)
  * Rice Core Framework (org.kuali.rice:rice-core-framework:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-core/rice-core-framework)
  * Rice Core Impl (org.kuali.rice:rice-core-impl:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-core/rice-core-impl)
  * Rice Core Service API (org.kuali.rice:rice-core-service-api:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-core-service/rice-core-service-api)
  * Rice Core Service Framework (org.kuali.rice:rice-core-service-framework:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-core-service/rice-core-service-framework)
  * Rice Core Service Impl (org.kuali.rice:rice-core-service-impl:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-core-service/rice-core-service-impl)
  * Rice Core Service Web (org.kuali.rice:rice-core-service-web:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-core-service/rice-core-service-web)
  * Rice Core Web (org.kuali.rice:rice-core-web:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-core/rice-core-web)
  * Rice EDL Framework (org.kuali.rice:rice-edl-framework:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-edl/rice-edl-framework)
  * Rice EDL Impl (org.kuali.rice:rice-edl-impl:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-edl/rice-edl-impl)
  * Rice Implementation (org.kuali.rice:rice-impl:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-impl)
  * Rice KEN API (org.kuali.rice:rice-ken-api:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-ken/rice-ken-api)
  * Rice KEW API (org.kuali.rice:rice-kew-api:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-kew/rice-kew-api)
  * Rice KEW Framework (org.kuali.rice:rice-kew-framework:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-kew/rice-kew-framework)
  * Rice KEW Impl (org.kuali.rice:rice-kew-impl:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-kew/rice-kew-impl)
  * Rice KIM API (org.kuali.rice:rice-kim-api:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-kim/rice-kim-api)
  * Rice KIM Framework (org.kuali.rice:rice-kim-framework:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-kim/rice-kim-framework)
  * Rice KIM Impl (org.kuali.rice:rice-kim-impl:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-kim/rice-kim-impl)
  * Rice LDAP Connector (org.kuali.rice:rice-kim-ldap:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-kim/rice-kim-ldap)
  * Rice KNS (org.kuali.rice:rice-kns:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-kns)
  * Rice KRAD Application Framework (org.kuali.rice:rice-krad-app-framework:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-framework/rice-krad-app-framework)
  * Rice KRAD data (org.kuali.rice:rice-krad-data:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-framework/rice-krad-data)
  * Rice KRAD Service Impl (org.kuali.rice:rice-krad-service-impl:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-framework/rice-krad-service-impl)
  * Rice KRAD Web (org.kuali.rice:rice-krad-web:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-framework/rice-krad-web)
  * Rice KRAD Web Framework (org.kuali.rice:rice-krad-web-framework:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-framework/rice-krad-web-framework)
  * Rice KRMS API (org.kuali.rice:rice-krms-api:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-krms/rice-krms-api)
  * Rice KRMS Framework (org.kuali.rice:rice-krms-framework:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-krms/rice-krms-framework)
  * Rice KRMS Impl (org.kuali.rice:rice-krms-impl:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-krms/rice-krms-impl)
  * Rice KSB API (org.kuali.rice:rice-ksb-api:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-ksb/rice-ksb-api)
  * Rice KSB Client Implementation (org.kuali.rice:rice-ksb-client-impl:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-ksb/rice-ksb-client-impl)
  * Rice KSB Server Implementation (org.kuali.rice:rice-ksb-server-impl:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-ksb/rice-ksb-server-impl)
  * Rice KSB Web Application (org.kuali.rice:rice-ksb-web:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-ksb/rice-ksb-web)
  * Rice Legacy Web (org.kuali.rice:rice-legacy-web:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-legacy-web)
  * Rice Location API (org.kuali.rice:rice-location-api:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-location/rice-location-api)
  * Rice Location Framework (org.kuali.rice:rice-location-framework:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-location/rice-location-framework)
  * Rice Location Impl (org.kuali.rice:rice-location-impl:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-location/rice-location-impl)
  * Rice Location Web (org.kuali.rice:rice-location-web:2.6.2 - http://site.kuali.org/rice/2.6.2/rice-middleware/rice-location/rice-location-web)

GNU General Public License - Version 2 : 
  * MySQL Connector/J (mysql:mysql-connector-java:5.1.41 - http://dev.mysql.com/doc/connector-j/en/)

GNU General Public License - Version 2 / CPE : 
  * javax.ws.rs-api (javax.ws.rs:javax.ws.rs-api:2.0.1 - http://jax-rs-spec.java.net)

GNU Lesser General Public License : 
  * c3p0:JDBC DataSources/Resource Pools (c3p0:c3p0:0.9.1.1 - http://c3p0.sourceforge.net)
  * Cryptacular Library (org.cryptacular:cryptacular:1.0 - http://www.cryptacular.org)
  * Jacorb (org.jacorb:jacorb:2.2.3-jonas-patch-20071018 - http://www.jacorb.org)
  * Jacorb IDL (org.jacorb:jacorb-idl:2.2.3-jonas-patch-20071018 - http://www.jacorb.org)

GNU Lesser General Public License - Version 2.1 : 
  * Javassist (org.javassist:javassist:3.21.0-GA - http://www.javassist.org/)
  * CAROL: Common Architecture for RMI OW2 Layer (org.ow2.carol:carol:3.0.6 - http://carol.ow2.org/carol-modules/carol)
  * CAROL: Interceptors (org.ow2.carol:carol-interceptors:1.0.1 - http://carol.ow2.org)

GNU Lesser General Public License - Version 3 : 
  * Bitronix Transaction Manager :: Core (org.codehaus.btm:btm:2.1.4 - http://docs.codehaus.org/display/BTM/Home/btm)
  * xapool (xapool:xapool:1.5.0-patch6 - no url defined)

GNU Lesser General Public License Version 2.1 : 
  * IRMI (org.ow2.carol.irmi:irmi:1.1.2 - no url defined)

Indiana University Extreme! Lab Software License, vesion 1.1.1 : 
  * MXP1: Xml Pull Parser 3rd Edition (XPP3) (xpp3:xpp3_min:1.1.4c - http://www.extreme.indiana.edu/xgws/xsoap/xpp/mxp1/)

MIT License : 
  * Mockito (org.mockito:mockito-core:1.10.19 - http://www.mockito.org)
  * JCL 1.2 implemented over SLF4J (org.slf4j:jcl-over-slf4j:1.7.24 - http://www.slf4j.org)
  * SLF4J API Module (org.slf4j:slf4j-api:1.7.24 - http://www.slf4j.org)
  * SLF4J LOG4J-12 Binding (org.slf4j:slf4j-log4j12:1.7.24 - http://www.slf4j.org)

Mozilla Public License - Version 1.1 : 
  * Javassist (org.javassist:javassist:3.21.0-GA - http://www.javassist.org/)

New BSD License : 
  * Hamcrest Core (org.hamcrest:hamcrest-core:1.3 - https://github.com/hamcrest/JavaHamcrest/hamcrest-core)

Public Domain : 
  * AOP alliance (aopalliance:aopalliance:1.0 - http://aopalliance.sourceforge.net)
  * XML Pull Parsing API (xmlpull:xmlpull:1.1.3.1 - http://www.xmlpull.org)
  * MXP1: Xml Pull Parser 3rd Edition (XPP3) (xpp3:xpp3_min:1.1.4c - http://www.extreme.indiana.edu/xgws/xsoap/xpp/mxp1/)

Similar to Apache License but with the acknowledgment clause removed : 
  * JDOM (org.jdom:jdom:1.1.3 - http://www.jdom.org)

Sun Binary License : 
  * connector-api (javax.resource:connector-api:1.5 - no url defined)
  * Java Transaction API (javax.transaction:jta:1.1 - http://java.sun.com/products/jta)

Unknown license : 
  * core-filter (co.kuali:core-filter:1.7 - no url defined)